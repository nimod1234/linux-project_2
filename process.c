#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(){
    
    pid_t pid[10];
    int status;
    int count=0;
    for(int i=0;i<10;i++){
        if((pid[i]=fork())>0){
            printf("child[%d] will be killed\n",pid[i]);
            kill(pid[i], SIGTERM);
            printf("Successful killing, waiting for the next child\n");
            sleep(1);
            wait(&status);
            count++;
        }
        else printf("fail to fork\n");
    }
    if(count==10) printf("Killed every child. :)\n");
    else printf("fail to Kill every child. :(\n");
    return 0;
}